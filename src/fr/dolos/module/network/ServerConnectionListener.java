package fr.dolos.module.network;

public interface ServerConnectionListener {
	public void onClientDisconnected(DolosNetworkClient client);
}
