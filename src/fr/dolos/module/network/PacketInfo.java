package fr.dolos.module.network;

import fr.dolos.sdk.network.Packet;

public class PacketInfo {

	private String packet;
	private String data;
	
	public PacketInfo()
	{
		
	}
	
	public PacketInfo(Packet p)
	{
		packet = p.getName();
		data = p.serialize();
	}
	
	public String getPacket() {
		return packet;
	}
	public void setPacket(String packet) {
		this.packet = packet;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
}
