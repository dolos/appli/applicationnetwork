package fr.dolos.module.network;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.dolos.sdk.models.Drone;
import fr.dolos.sdk.network.Filter;
import fr.dolos.sdk.network.NetworkClient;
import fr.dolos.sdk.network.Packet;
import fr.dolos.sdk.network.PacketInstantiater;

public class DolosNetworkClient implements NetworkClient {

	private final static String NETWORK_MSG_START = "<Dolos_STR>"; // Start indicator
	private final static String NETWORK_MSG_END = "</Dolos_STR>"; // End indicator
	private final static int BUFFER_SIZE = 30;

	private Filter packetFilter = null;
	private PacketInstantiater instantiater = null;
	private boolean connected = false;
	private ServerConnectionListener listener;
	private StringBuilder dataBuilder;
	private final Socket socket;
	private final ObjectMapper mapper;
	private Drone drone;

	public DolosNetworkClient(Filter packetFilter, PacketInstantiater instantiater, ServerConnectionListener listener)
	{
		if (packetFilter == null || instantiater == null || listener == null)
			throw new NullPointerException();
		this.mapper = new ObjectMapper();
		this.listener = listener;
		this.packetFilter = packetFilter;
		this.instantiater = instantiater;
		this.socket = new Socket();
		this.dataBuilder = new StringBuilder();
	}
	
	public void setDrone(Drone d)
	{
		this.drone = d;
	}
	
	public Drone getDrone()
	{
		return drone;
	}

	public boolean connect(String ip, int port)
	{
		SocketAddress address = new InetSocketAddress(ip, port);
		try {
			socket.connect(address);
			connected = true;
		} catch (IOException e) {
			e.printStackTrace();
			return (false);
		}
		return (true);
	}

	public void setPacketFilter(Filter packetFilter) {
		if (packetFilter == null)
			throw new NullPointerException();
		this.packetFilter = packetFilter;
	}

	public void setInstantiater(PacketInstantiater instantiater) {
		if (instantiater == null)
			throw new NullPointerException();
		this.instantiater = instantiater;
	}

	protected void deserializeData(String rawFiltered)
	{
		rawFiltered = packetFilter.onDataReceiving(rawFiltered);
		try {
			PacketInfo info = mapper.readValue(rawFiltered, PacketInfo.class);
			Packet p = instantiater.instantiate(info.getPacket(), info.getData());
			if (p == null)
				return;
			packetFilter.onPacketReceiving(p);
			instantiater.onPacketReady(p, this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @return True if socket is still active
	 */
	public boolean update()
	{
		try {
			if (socket.isClosed())
				return (false);
			socket.setSoTimeout(MainModule.SO_TIMEOUT);
			InputStream stream = socket.getInputStream();
			byte buffer[] = new byte[BUFFER_SIZE];
			int length = stream.read(buffer);
			if (length == -1)
				return (false); // Stream has been remotely closed
			dataBuilder.append(new String(buffer, "UTF-8").substring(0, length));
			if (dataBuilder.toString().contains(DolosNetworkClient.NETWORK_MSG_END))
			{
				// Get data between NETWORK_MSG_START and NETWORK_MSG_END including NETWORK_MSG_START/END
				String raw = dataBuilder.substring(dataBuilder.indexOf(NETWORK_MSG_START), dataBuilder.indexOf(NETWORK_MSG_END) + NETWORK_MSG_END.length());
				dataBuilder.delete(dataBuilder.indexOf(raw), raw.length()); // Delete data from builder
				String rawFiltered = raw.substring(NETWORK_MSG_START.length(), raw.indexOf(NETWORK_MSG_END));
				deserializeData(rawFiltered);
			}
		}
		catch (SocketTimeoutException e) {} // Ignore timeout
		catch (IOException e) {
			e.printStackTrace();
			return (false);
		}
		return (true);
	}

	/**
	 * Close only the socket, doesn't use listener callback
	 */
	public void closeSocket()
	{
		if (!connected)
			return;
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		connected = false;
	}

	@Override
	public void close() {
		this.closeSocket();
		listener.onClientDisconnected(this);
	}

	@Override
	public boolean isConnected() {
		return connected;
	}

	protected <T extends Packet> String serialize(T packet) throws JsonProcessingException
	{
		packetFilter.onPacketSending(packet);
		PacketInfo info = new PacketInfo(packet);
		String serialized = mapper.writeValueAsString(info);
		serialized = packetFilter.onDataSending(serialized);
		return (serialized);
	}

	@Override
	public <T extends Packet> void send(T packet) throws IOException {
		if (!socket.isConnected())
			throw new IOException("Socket is not opened");
		try {
			String serialized = new StringBuilder()
					.append(NETWORK_MSG_START)
					.append(this.serialize(packet))
					.append(NETWORK_MSG_END)
					.toString();
			System.out.println(String.format("Sending packet '%s'", serialized));
			socket.getOutputStream().write(serialized.getBytes("UTF-8"));
		} catch (JsonProcessingException e)
		{
			throw new IOException("NetworkModule: Unable to parse packet json information");
		}
	}

    @Override
    public String getIp() {
        return socket.getInetAddress().getHostAddress();
    }
}
