package fr.dolos.module.network;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import fr.dolos.sdk.Core;
import fr.dolos.sdk.models.Drone;
import fr.dolos.sdk.modules.SystemManager;
import fr.dolos.sdk.modules.SystemModule;
import fr.dolos.sdk.network.ConnectionInstantiater;
import fr.dolos.sdk.network.ConnectionListener;
import fr.dolos.sdk.network.Filter;
import fr.dolos.sdk.network.NetworkClient;
import fr.dolos.sdk.network.PacketInstantiater;

public class MainModule extends SystemModule implements ConnectionInstantiater, ServerConnectionListener {

	private static final String MODULE_NAME = "Dolos_Network";
	public static final int SO_TIMEOUT = 50;

	private PacketInstantiater instantiater = null;
	private Filter applicativeFilter = null;
	private Core core = null;
	private ConnectionListener listener = null;
	private final List<DolosNetworkClient> clients = new ArrayList<DolosNetworkClient>();

	@Override
	public boolean load(Core core, SystemManager manager) {
		this.core = core;
		manager.setConnectionInstantiater(this);
		return true;
	}

	@Override
	public void unload() {
		Iterator<DolosNetworkClient> it = clients.iterator();
		while (it.hasNext())
		{
			DolosNetworkClient c = it.next();
			c.closeSocket();
			it.remove();
			listener.onClientDisconnected(c.getDrone());
		}
	}

	@Override
	public String getName() {
		return MODULE_NAME;
	}

	@Override
	public void update() {
		Iterator<DolosNetworkClient> it = clients.iterator();
		while (it.hasNext())
		{
			DolosNetworkClient c = it.next();
			if (!c.update())
			{
				c.closeSocket();
				it.remove();
				listener.onClientDisconnected(c.getDrone());
			}
		}
	}

	// INetworkServer implementation

	@Override
	public void setPacketInstantiater(PacketInstantiater instantiater) {
		this.instantiater = instantiater;
		for (DolosNetworkClient client : clients)
			client.setInstantiater(instantiater);
	}

	@Override
	public void setFilterApplicative(Filter filter) {
		this.applicativeFilter = filter;
		for (DolosNetworkClient client : clients)
			client.setPacketFilter(filter);
	}

	@Override
	public void setConnectionListener(ConnectionListener listener) {
		if (listener == null)
			throw new NullPointerException(String.format("%s: ConnectionListener can't be null", getName()));
		this.listener = listener;
	}

	@Override
	public Future<NetworkClient> connectTo(String ip, int port, int id) {
		return core.getScheduler().runAsync(new Callable<NetworkClient>() {
			@Override
			public NetworkClient call() throws Exception {
				DolosNetworkClient client = new DolosNetworkClient(applicativeFilter, instantiater, MainModule.this);
				if (!client.connect(ip, port))
					return (null);
				client.setDrone(new Drone(client, id, port, ip));
				synchronized (clients)
				{
					clients.add(client);
				}
				listener.onClientReady(client.getDrone());
				return client;
			}
		});
	}

	@Override
	public void onClientDisconnected(DolosNetworkClient client) {
		clients.remove(client);
		listener.onClientDisconnected(client.getDrone());
	}
}
